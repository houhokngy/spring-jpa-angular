package info.houhokngy.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJpaAngularApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringJpaAngularApplication.class, args);
	}
}
